package com.example.thuan_ps10375_ass_final_du_an_mau;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;


import com.example.thuan_ps10375_ass_final_du_an_mau.Adapter.KhachHangAdapter;
import com.example.thuan_ps10375_ass_final_du_an_mau.DAO.KhachHangDAO;
import com.example.thuan_ps10375_ass_final_du_an_mau.Model.KhachHang;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.ScaleInBottomAnimator;

public class Fragment_KhachHang extends Fragment {

    KhachHangDAO khachhangDAO;
    ArrayList<KhachHang> list;
    KhachHangAdapter khachhangAdapter;
    public Fragment_KhachHang(){}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      View view = inflater.inflate(R.layout.khachhang_fragment,container,false);


        FloatingActionButton fab = view.findViewById(R.id.fab);
        list = new ArrayList<>();
        khachhangDAO = new KhachHangDAO(getActivity());
        list = khachhangDAO.getAllkhachhang();
        khachhangAdapter = new KhachHangAdapter(list, getContext());
        // Thêm recyclerView vào
        final RecyclerView recyclerView = view.findViewById(R.id.rv_khachhang);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new ScaleInBottomAnimator());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(khachhangAdapter);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = Fragment_KhachHang.this.getLayoutInflater();
                v = inflater.inflate(R.layout.alert_khachhang, null);
                builder.setView(v);
                final EditText edttenkh = v.findViewById(R.id.edttenkh);
                final EditText edtsdtkh = v.findViewById(R.id.edtsdtkh);
                final EditText edtmatkhaukh = v.findViewById(R.id.edtmatkhaukh);

                builder.setPositiveButton("Thêm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String sodienthoai = edtsdtkh.getText().toString();
                        Boolean checksdt = khachhangDAO.checksdtkhachhang(sodienthoai);
                        if (edttenkh.getText().length() == 0 ||
                                edtsdtkh.getText().length() == 0 ||
                                edtmatkhaukh.getText().length() == 0) {
                            Toast.makeText(getContext(), "Chưa nhập thông tin", Toast.LENGTH_LONG).show();
                        } else if (checksdt == false) {
                            Toast.makeText(getContext(), "Số điện thoại đã tồn tại", Toast.LENGTH_LONG).show();
                            return;
                        } else {
                            KhachHang kh = new KhachHang();
                            kh.setTenkh(edttenkh.getText().toString());
                            kh.setSdtkh(edtsdtkh.getText().toString());
                            kh.setMatkhaukh(edtmatkhaukh.getText().toString());
                            if (khachhangDAO.addKhachHang(kh) == -1) {
                                Toast.makeText(getContext(), "Thêm không thành công", Toast.LENGTH_LONG).show();
                                return;
                            }
                            Toast.makeText(getContext(), "Thêm " + edtsdtkh.getText().toString() + " thành công", Toast.LENGTH_SHORT).show();
                            list = khachhangDAO.getAllkhachhang();
                            khachhangAdapter = new KhachHangAdapter(list, getContext());
                            recyclerView.setAdapter(khachhangAdapter);
                        }
                    }
                });
               builder.setNegativeButton("Thoát", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });
        return view;
    }


}
