package com.example.thuan_ps10375_ass_final_du_an_mau;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.thuan_ps10375_ass_final_du_an_mau.Adapter.Tablayout_Sach;

public class Fragment_Sach extends Fragment {
    TabLayout tabLayout;
    ViewPager viewPager;
    public Fragment_Sach(){}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sach_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tabLayout = view.findViewById(R.id.tab_layoutsach);
        viewPager = view.findViewById(R.id.viewpager);
        viewPager.setAdapter(new Tablayout_Sach(getChildFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
    }
}
