package com.example.thuan_ps10375_ass_final_du_an_mau;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.thuan_ps10375_ass_final_du_an_mau.DAO.HoaDonChiTietDAO;
public class Fragment_TK extends Fragment {
    TextView tvNgay, tvThang, tvNam;
    HoaDonChiTietDAO hoaDonChiTietDAO;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.thongke_fragment, container, false);

        tvNgay = view.findViewById(R.id.tvThongKeNgay);
        tvThang = view.findViewById(R.id.tvThongKeThang);
        tvNam = view.findViewById(R.id.tvThongKeNam);
        hoaDonChiTietDAO = new HoaDonChiTietDAO(getContext());


       /* tvNgay.setText("Hôm nay:   " + hoaDonChiTietDAO.getDoanhThuTheoNgay());
        tvThang.setText("Tháng này: " + hoaDonChiTietDAO.getDoanhThuTheoThang());
        tvNam.setText("Năm này:   " + hoaDonChiTietDAO.getDoanhThuTheoNam());
*/

        return view;
    }

}