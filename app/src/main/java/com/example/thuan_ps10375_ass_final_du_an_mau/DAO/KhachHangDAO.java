package com.example.thuan_ps10375_ass_final_du_an_mau.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import com.example.thuan_ps10375_ass_final_du_an_mau.Database.DBHelper;
import com.example.thuan_ps10375_ass_final_du_an_mau.Model.KhachHang;

import java.util.ArrayList;

public class KhachHangDAO {
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    public static final String SQL_KHACHHANG="CREATE TABLE KHACHHANG(tenkhachhang text," +
            "sodienthoai text primary key, matkhau varchar not null)";
    public KhachHangDAO(Context context) {
        dbHelper = new DBHelper(context);
        db = dbHelper.getWritableDatabase();
    }
    // Thêm khách hàng
        public long addKhachHang(KhachHang kh){
            SQLiteDatabase database;
            db=dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("tenkhachhang",kh.getTenkh());
            values.put("sodienthoai", kh.getSdtkh());
            values.put("matkhau", kh.getMatkhaukh());
           return db.insert("Khachhang", null, values);
            }

         // Lấy tất cả khách hàng
    public ArrayList<KhachHang> getAllkhachhang(){
        ArrayList<KhachHang> danhsachkhachhang = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cr = db.query("KhachHang", null, null, null, null, null, null);
        //Cách 2
        // String sql = "SELECT * FORM KhachHang;
        // Cursor cr = db.rawQuery(sql, null);
        cr.moveToFirst();
        while (!cr.isAfterLast()){
            String tenkhachhang = cr.getString(0);
            String sodienthoai = cr.getString(1);
            String matkhau = cr.getString(2);
            danhsachkhachhang.add(new KhachHang(tenkhachhang, sodienthoai, matkhau));
            cr.moveToNext();
        }return danhsachkhachhang;
        }

        // check trùng sdt khi thêm vào database
        public Boolean checksdtkhachhang(String sodienthoai){
            SQLiteDatabase db;
            db = dbHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("Select * from khachhang where sodienthoai=?", new String[]{sodienthoai});
            if(cursor.getCount()>0) return false;
            else return true;
            }
        // Cập nhật khách hàng
    public int updateKhachHang(KhachHang kh){
        SQLiteDatabase db;
        db = dbHelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put("tenkhachhang",kh.getTenkh());
        values.put("sodienthoai", kh.getSdtkh());
        values.put("matkhau", kh.getMatkhaukh());
        return  db.update("KhachHang", values, "sodienthoai=?", new
                String[]{kh.getSdtkh()});

    }

    // Xóa khách hàng
    public int deleteKhachHang(KhachHang kh){
        return  db.delete("Khachhang", "sodienthoai=?", new
                String[]{String.valueOf(kh.getSdtkh())});
    }

    //Kiểm tra đăng nhập
    public boolean checkLoginKhachHang(String sodienthoai, String matkhau){
        SQLiteDatabase db;
        db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM KHACHHANG WHERE sodienthoai=? AND matkhau=?",
               new String[]{sodienthoai, matkhau});
        if(cursor.getCount()>0)
            return true;
        else  return false;
        }

}
