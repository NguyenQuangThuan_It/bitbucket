package com.example.thuan_ps10375_ass_final_du_an_mau.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import com.example.thuan_ps10375_ass_final_du_an_mau.Database.DBHelper;
import com.example.thuan_ps10375_ass_final_du_an_mau.Model.TheLoai;

import java.util.ArrayList;

public class TheLoaiDAO {
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    public static final String SQL_THELOAI="CREATE TABLE THELOAI(matheloai text primary key," +
    "tentheloai text, vitri text)";
    public TheLoaiDAO(Context context){
        dbHelper = new DBHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    public long addTheLoai(TheLoai tl){
        SQLiteDatabase database;
        db=dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("matheloai",tl.getMaTheLoai());
        values.put("tentheloai", tl.getTenTheLoai());
        values.put("vitri", tl.getViTri());
        return db.insert("THELOAI", null, values);
    }

    public ArrayList<TheLoai> getAllTheLoai(){
        ArrayList<TheLoai> danhsachtheloai = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cr = db.query("THELOAI", null, null, null, null, null, null);
        //Cách 2
        // String sql = "SELECT * FORM TheLoai;
        // Cursor cr = db.rawQuery(sql, null);
        cr.moveToFirst();
        while (!cr.isAfterLast()){
            String matheloai = cr.getString(0);
            String tentheloai = cr.getString(1);
            String vitri = cr.getString(2);
            danhsachtheloai.add(new TheLoai(matheloai, tentheloai, vitri));
            cr.moveToNext();
        }return danhsachtheloai;
    }

    public int updateTheLoai(TheLoai tl){
        SQLiteDatabase db;
        db = dbHelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put("matheloai",tl.getMaTheLoai());
        values.put("tentheloai", tl.getTenTheLoai());
        values.put("vitri", tl.getViTri());
        return  db.update("TheLoai", values, "matheloai=?", new
                String[]{tl.getMaTheLoai()});
    }
}
