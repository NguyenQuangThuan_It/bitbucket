package com.example.thuan_ps10375_ass_final_du_an_mau.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import com.example.thuan_ps10375_ass_final_du_an_mau.Database.DBHelper;
import com.example.thuan_ps10375_ass_final_du_an_mau.Model.HoaDon;

import java.util.ArrayList;

public class HoaDonDAO {
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    public static final String SQL_HOADON="CREATE TABLE HOADON(mahoadon text primary key," +
            "ngaythemdon date," +
            "slsachmua text," +
            "masach text, foreign key (masach) references SACH(masach))";
    public HoaDonDAO(Context context) {
        dbHelper = new DBHelper(context);
        db = dbHelper.getWritableDatabase();
    }
    public long addhoadon(HoaDon hd){
        SQLiteDatabase database;
        db=dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("mahoadon",hd.getMahoadon());
        values.put("ngaythemdon", hd.getNgaythemdon());
        values.put("slsachmua", hd.getSlsachmua());
        values.put("masach", hd.getMasach());
        return db.insert("HoaDon", null, values);
    }

    public ArrayList<HoaDon> getAllhoadon() {
        ArrayList<HoaDon> danhsachhoadon = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cr = db.query("HoaDon", null, null, null, null, null, null);
        //Cách 2
        // String sql = "SELECT * FORM TheLoai;
        // Cursor cr = db.rawQuery(sql, null);
        cr.moveToFirst();
        while (!cr.isAfterLast()){
            String mahoadon = cr.getString(0);
            String ngaythemdon = cr.getString(1);
            String slsachmua = cr.getString(2);
            String masach = cr.getString(3);
            danhsachhoadon.add(new HoaDon(mahoadon, ngaythemdon,slsachmua, masach));
            cr.moveToNext();
        }
        return danhsachhoadon;
    }

    public int updateHoaDon(HoaDon hd){
        SQLiteDatabase db;
        db = dbHelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put("masach",hd.getMasach());
        values.put("mahoadon",hd.getMahoadon());
        values.put("ngaythemdon",hd.getNgaythemdon());
        values.put("slsachmua",hd.getSlsachmua());
        return  db.update("HoaDon", values, "mahoadon=?", new
                String[]{hd.getMahoadon()});
    }

    public Boolean checkhoadon(String mahoadon){
        SQLiteDatabase db;
        db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from hoadon where mahoadon=?", new String[]{mahoadon});
        if(cursor.getCount()>0) return false;
        else return true;
    }

    public int delehoadon(HoaDon sa){
        return  db.delete("HoaDon", "mahoadon=?", new
                String[]{String.valueOf(sa.getMahoadon())});
    }
}
