package com.example.thuan_ps10375_ass_final_du_an_mau.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.thuan_ps10375_ass_final_du_an_mau.Database.DBHelper;
import com.example.thuan_ps10375_ass_final_du_an_mau.Model.Sach;

import java.util.ArrayList;

public class SachDAO {
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    public static final String SQL_SACH="CREATE TABLE SACH(masach text primary key,"
            + "tensach text,"
            + "giabia text,"
            + "soluong text,"
            + "matheloai text, foreign key (matheloai) references THELOAI(matheloai))";
    public SachDAO(Context context) {
        dbHelper = new DBHelper(context);
        db = dbHelper.getWritableDatabase();
}
    public long addsach(Sach sa){
        SQLiteDatabase database;
        db=dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("masach",sa.getMasach());
        values.put("tensach", sa.getTensach());
        values.put("giabia", sa.getGiabia());
        values.put("soluong", sa.getSoluong());
        values.put("matheloai", sa.getMatheloai());
        return db.insert("SACH", null, values);
    }

    public ArrayList<Sach> laytatcasach() {
        ArrayList<Sach> danhsachsach = new ArrayList<>();
        Cursor cr = db.query("Sach", null, null,null,null,null,null);
        while(cr.moveToNext()){
            Sach sa = new Sach();
            sa.setMasach(cr.getString(0));
            sa.setTensach(cr.getString(1));
            sa.setGiabia(cr.getString(2));
            sa.setSoluong(cr.getString(3));
            sa.setMatheloai(cr.getString(4));
            danhsachsach.add(sa);
        }
        return danhsachsach;
    }

    public int updateSach(Sach sa){
        SQLiteDatabase db;
        db = dbHelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put("masach",sa.getMasach());
        values.put("tensach",sa.getTensach());
        values.put("giabia",sa.getGiabia());
        values.put("soluong",sa.getSoluong());
        values.put("matheloai",sa.getMatheloai());
        return  db.update("Sach", values, "masach=?", new
                String[]{sa.getMasach()});
    }
    public Boolean checksach(String masach){
        SQLiteDatabase db;
        db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from sach where masach=?", new String[]{masach});
        if(cursor.getCount()>0) return false;
        else return true;
    }

    public int deletesach(Sach sa){
        return  db.delete("Sach", "masach=?", new
                String[]{String.valueOf(sa.getMasach())});
    }


}
