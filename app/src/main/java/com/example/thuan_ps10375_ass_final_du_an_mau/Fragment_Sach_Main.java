package com.example.thuan_ps10375_ass_final_du_an_mau;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.thuan_ps10375_ass_final_du_an_mau.Adapter.SachAdapter;
import com.example.thuan_ps10375_ass_final_du_an_mau.Adapter.SpinnerAdapter;
import com.example.thuan_ps10375_ass_final_du_an_mau.Adapter.TheLoaiAdapter;
import com.example.thuan_ps10375_ass_final_du_an_mau.DAO.SachDAO;
import com.example.thuan_ps10375_ass_final_du_an_mau.DAO.TheLoaiDAO;
import com.example.thuan_ps10375_ass_final_du_an_mau.Model.Sach;
import com.example.thuan_ps10375_ass_final_du_an_mau.Model.TheLoai;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.ScaleInBottomAnimator;

public class Fragment_Sach_Main extends Fragment {
    ArrayList<Sach> list_sach;
    ArrayList<TheLoai> list_theloai;
    SachDAO sachDAO;
    EditText edtmasosach, edttensach, edtsoluongsach, edtgiasach;
    TheLoaiDAO theloaiDAO;
    SachAdapter sachAdapter;
    TheLoaiAdapter theloaiAdapter;
    FloatingActionButton fabsach;
    RecyclerView rv_sach;
    Spinner spinner_themsach;
    android.widget.SpinnerAdapter spinnerAdapter;

    TheLoai tl = new TheLoai();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sachmain_fragment, container, false);
        // thể loại
        list_sach = new ArrayList<>();
        list_theloai = new ArrayList<>();
        theloaiDAO = new TheLoaiDAO(getContext());
        list_theloai = theloaiDAO.getAllTheLoai();
        theloaiAdapter = new TheLoaiAdapter(list_theloai, getContext());
        sachDAO = new SachDAO(getContext());
        list_sach = sachDAO.laytatcasach();
        sachAdapter = new SachAdapter(list_sach, getContext());
        //
        fabsach = view.findViewById(R.id.fabsach);
        spinnerAdapter = new SpinnerAdapter(getActivity(), list_theloai);

        //recycler view
        rv_sach = view.findViewById(R.id.rv_sach);
        rv_sach.setHasFixedSize(true);
        rv_sach.setItemAnimator(new ScaleInBottomAnimator());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rv_sach.setLayoutManager(layoutManager);
        rv_sach.setAdapter(sachAdapter);

        fabsach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = Fragment_Sach_Main.this.getLayoutInflater();
                v = inflater.inflate(R.layout.alert_sach, null);
                builder.setView(v);

                //----------------------------------------
                edttensach = v.findViewById(R.id.edttensach);
                edtmasosach = v.findViewById(R.id.edtmasosach);
                edtsoluongsach = v.findViewById(R.id.edtsoluongsach);
                edtgiasach = v.findViewById(R.id.edtgiasach);
                spinner_themsach = v.findViewById(R.id.spinner_themsach);
                spinnerAdapter = new SpinnerAdapter(getActivity(), list_theloai);
                spinner_themsach.setAdapter(spinnerAdapter);
                //--------------------------------------
                list_sach = new ArrayList<>();
                sachDAO = new SachDAO(getActivity());
                builder.setPositiveButton("Thêm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String masach = edtmasosach.getText().toString();
                        Boolean checksach = sachDAO.checksach(masach);
                        if (spinner_themsach.getSelectedItem().toString().length() == 0 || edtmasosach.length() == 0 || edttensach.length() == 0
                                || edtsoluongsach.length() == 0 || edtgiasach.length() == 0) {
                            Toast.makeText(getActivity(), "Chưa nhập đủ thông tin", Toast.LENGTH_SHORT).show();
                        } else if (checksach == false) {
                            Toast.makeText(getContext(), "Mã sách đã tồn tại", Toast.LENGTH_LONG).show();
                            return;
                        } else {
                            Sach sach = new Sach();
                            sach.setMatheloai(spinner_themsach.getSelectedItem().toString());
                            sach.setMasach(edtmasosach.getText().toString());
                            sach.setTensach(edttensach.getText().toString());
                            sach.setSoluong(edtsoluongsach.getText().toString());
                            sach.setGiabia(edtgiasach.getText().toString());
                            if (sachDAO.addsach(sach) == -1) {
                                Toast.makeText(getActivity(), "Thêm sách không thành công", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Toast.makeText(getActivity(), "Thêm sách " + edttensach.getText().toString() + " thành công", Toast.LENGTH_SHORT).show();
                            list_sach = sachDAO.laytatcasach();
                            sachAdapter = new SachAdapter(list_sach, getContext());
                            rv_sach.setAdapter(sachAdapter);
                        }
                    }
                });
                builder.show();
            }
               });

        return view;

    }

}