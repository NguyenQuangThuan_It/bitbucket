package com.example.thuan_ps10375_ass_final_du_an_mau.Model;

public class HoaDon {
    public HoaDon(String mahoadon, String ngaythemdon, String slsachmua, String masach) {
        this.mahoadon = mahoadon;
        this.ngaythemdon = ngaythemdon;
        this.slsachmua = slsachmua;
        this.masach = masach;

    }
    public HoaDon(){}

    public String getMahoadon() {
        return mahoadon;
    }

    public void setMahoadon(String mahoadon) {
        this.mahoadon = mahoadon;
    }

    public String getNgaythemdon() {
        return ngaythemdon;
    }

    public void setNgaythemdon(String ngaythemdon) {
        this.ngaythemdon = ngaythemdon;
    }

    private String mahoadon;
    private String ngaythemdon;
    private String slsachmua;
    private String masach;

    public String getMasach() {
        return masach;
    }

    public void setMasach(String masach) {
        this.masach = masach;
    }

    public String getSlsachmua() {
        return slsachmua;
    }

    public void setSlsachmua(String slsachmua) {
        this.slsachmua = slsachmua;
    }


}
