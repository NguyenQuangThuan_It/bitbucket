package com.example.thuan_ps10375_ass_final_du_an_mau.Model;

import android.support.annotation.NonNull;

public class TheLoai {
    public TheLoai(String maTheLoai, String tenTheLoai, String viTri) {
        this.maTheLoai = maTheLoai;
        this.tenTheLoai = tenTheLoai;
        this.viTri = viTri;
    }

    public String getMaTheLoai() {
        return maTheLoai;
    }

    public void setMaTheLoai(String maTheLoai) {
        this.maTheLoai = maTheLoai;
    }

    public String getTenTheLoai() {
        return tenTheLoai;
    }

    public void setTenTheLoai(String tenTheLoai) {
        this.tenTheLoai = tenTheLoai;
    }

    public String getViTri() {
        return viTri;
    }

    public void setViTri(String viTri) {
        this.viTri = viTri;
    }

    private String maTheLoai;
    private String tenTheLoai;
    private String viTri;

    public TheLoai(){}

    @NonNull
    @Override
    public String toString() {
        return getMaTheLoai() + " | " + getTenTheLoai();
    }
}
