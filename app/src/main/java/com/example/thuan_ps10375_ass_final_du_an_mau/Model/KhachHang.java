package com.example.thuan_ps10375_ass_final_du_an_mau.Model;

public class KhachHang {
    public KhachHang(String tenkh, String sdtkh, String matkhaukh) {
        this.tenkh = tenkh;
        this.sdtkh = sdtkh;
        this.matkhaukh = matkhaukh;
    }

    public KhachHang(){}
    public String getTenkh() {
        return tenkh;
    }

    public void setTenkh(String tenkh) {
        this.tenkh = tenkh;
    }

    public String getSdtkh() {
        return sdtkh;
    }

    public void setSdtkh(String sdtkh) {
        this.sdtkh = sdtkh;
    }

    private String tenkh;
    private String sdtkh;

    public String getMatkhaukh() {
        return matkhaukh;
    }

    public void setMatkhaukh(String matkhaukh) {
        this.matkhaukh = matkhaukh;
    }

    private String matkhaukh;
}
