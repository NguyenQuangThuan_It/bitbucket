package com.example.thuan_ps10375_ass_final_du_an_mau.Model;

public class Sach {
    private String masach;

    public Sach(String masach, String tensach, String matheloai, String soluong, String giabia) {
        this.masach = masach;
        this.tensach = tensach;
        this.matheloai = matheloai;
        this.soluong = soluong;
        this.giabia = giabia;
    }

    private String tensach;
    public Sach(){}

    public String getMasach() {
        return masach;
    }

    public void setMasach(String masach) {
        this.masach = masach;
    }

    public String getTensach() {
        return tensach;
    }

    public void setTensach(String tensach) {
        this.tensach = tensach;
    }

    public String getMatheloai() {
        return matheloai;
    }

    public void setMatheloai(String matheloai) {
        this.matheloai = matheloai;
    }

    public String getSoluong() {
        return soluong;
    }

    public void setSoluong(String soluong) {
        this.soluong = soluong;
    }

    public String getGiabia() {
        return giabia;
    }

    public void setGiabia(String giabia) {
        this.giabia = giabia;
    }

    private String matheloai;
    private String soluong;
    private String giabia;
}
