package com.example.thuan_ps10375_ass_final_du_an_mau.Model;

public class HoaDonChiTiet {
    private int mahoadonCT;

    public int getMahoadonCT() {
        return mahoadonCT;
    }

    public void setMahoadonCT(int mahoadonCT) {
        this.mahoadonCT = mahoadonCT;
    }

    public String getMahoadon() {
        return mahoadon;
    }

    public void setMahoadon(String mahoadon) {
        this.mahoadon = mahoadon;
    }

    public String getMasachmua() {
        return masachmua;
    }

    public void setMasachmua(String masachmua) {
        this.masachmua = masachmua;
    }

    public String getSoluongmua() {
        return soluongmua;
    }

    public void setSoluongmua(String soluongmua) {
        this.soluongmua = soluongmua;
    }

    private String mahoadon;
    private String masachmua;

    public HoaDonChiTiet(int mahoadonCT, String mahoadon, String masachmua, String soluongmua) {
        this.mahoadonCT = mahoadonCT;
        this.mahoadon = mahoadon;
        this.masachmua = masachmua;
        this.soluongmua = soluongmua;
    }

    private String soluongmua;
    public HoaDonChiTiet(){}
}
