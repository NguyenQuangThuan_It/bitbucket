package com.example.thuan_ps10375_ass_final_du_an_mau.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.thuan_ps10375_ass_final_du_an_mau.DAO.HoaDonDAO;
import com.example.thuan_ps10375_ass_final_du_an_mau.DAO.KhachHangDAO;
import com.example.thuan_ps10375_ass_final_du_an_mau.DAO.SachDAO;
import com.example.thuan_ps10375_ass_final_du_an_mau.DAO.TheLoaiDAO;


public class DBHelper extends SQLiteOpenHelper {
    public static final String DBNAME= "BOOKMANAGER";
    public static final int VERSION= 1;
    public DBHelper(Context context){
        super(context,DBNAME, null,VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TheLoaiDAO.SQL_THELOAI);
        db.execSQL(KhachHangDAO.SQL_KHACHHANG);
        db.execSQL("INSERT INTO KHACHHANG VALUES('Tài khoản', 'admin', 'admin')");
        db.execSQL(SachDAO.SQL_SACH);
        db.execSQL(HoaDonDAO.SQL_HOADON);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(TheLoaiDAO.SQL_THELOAI);
    db.execSQL(KhachHangDAO.SQL_KHACHHANG);
    db.execSQL(SachDAO.SQL_SACH);
    db.execSQL(HoaDonDAO.SQL_HOADON);
    onCreate(db);
    }
}
