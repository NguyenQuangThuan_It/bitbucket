package com.example.thuan_ps10375_ass_final_du_an_mau;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.thuan_ps10375_ass_final_du_an_mau.Adapter.TheLoaiAdapter;
import com.example.thuan_ps10375_ass_final_du_an_mau.DAO.TheLoaiDAO;
import com.example.thuan_ps10375_ass_final_du_an_mau.Model.TheLoai;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.ScaleInBottomAnimator;

public class Fragment_TheLoai extends Fragment {
    TheLoaiDAO theloaiDAO;
    ArrayList<TheLoai> list;
    TheLoaiAdapter theloaiAdapter;
    FloatingActionButton fabtheloai;
    public Fragment_TheLoai(){}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View  view = inflater.inflate(R.layout.theloai_fragment,container,false);

        list = new ArrayList<>();
        theloaiDAO = new TheLoaiDAO(getActivity());
        list = theloaiDAO.getAllTheLoai();
        theloaiAdapter = new TheLoaiAdapter(list, getContext());
        fabtheloai = view.findViewById(R.id.fabtheloai);
        final RecyclerView recyclerView = view.findViewById(R.id.rv_theloai);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new ScaleInBottomAnimator());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(theloaiAdapter);

        fabtheloai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = Fragment_TheLoai.this.getLayoutInflater();
                v = inflater.inflate(R.layout.alert_theloai, null);
                builder.setView(v);
                final EditText edttentheloai = v.findViewById(R.id.edttentheloai);
                final EditText edtmatheloai = v.findViewById(R.id.edtmatheloai);
                final  EditText edtvitri = v.findViewById(R.id.edtvitri);
                builder.setPositiveButton("Thêm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (edttentheloai.getText().length() == 0 ||
                                edtvitri.getText().length() == 0 ||
                                edtmatheloai.getText().length() == 0) {
                            Toast.makeText(getContext(), "Chưa nhập thông tin", Toast.LENGTH_LONG).show();
                        } else {
                            TheLoai tl = new TheLoai();
                            tl.setMaTheLoai(edtmatheloai.getText().toString());
                            tl.setTenTheLoai(edttentheloai.getText().toString());
                            tl.setViTri(edtvitri.getText().toString());
                            if (theloaiDAO.addTheLoai(tl) == -1) {
                                Toast.makeText(getContext(), "Thêm không thành công", Toast.LENGTH_LONG).show();
                                return;
                            }
                            Toast.makeText(getContext(), "Thêm " + edttentheloai.getText().toString() + " thành công", Toast.LENGTH_SHORT).show();
                            list = theloaiDAO.getAllTheLoai();
                            theloaiAdapter = new TheLoaiAdapter(list, getContext());
                            recyclerView.setAdapter(theloaiAdapter);
                        }
                    }
                });
               builder.setNegativeButton("Thoát", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });
        return view;

    }


}
