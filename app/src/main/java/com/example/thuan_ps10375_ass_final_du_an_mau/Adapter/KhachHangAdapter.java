package com.example.thuan_ps10375_ass_final_du_an_mau.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thuan_ps10375_ass_final_du_an_mau.DAO.KhachHangDAO;
import com.example.thuan_ps10375_ass_final_du_an_mau.Model.KhachHang;
import com.example.thuan_ps10375_ass_final_du_an_mau.R;

import java.util.ArrayList;

public class KhachHangAdapter extends RecyclerView.Adapter<KhachHangAdapter.ViewHolder>{
    ArrayList<KhachHang> danhsachkh;
    Context context;
    KhachHangDAO khachhangDAO;
    TextView tveditsdtkh;
    EditText edtedittenkh, edteditmatkhaukh,edteditrematkhaukh;
    private  static View.OnClickListener listener;
    public  interface OnItemClickListener{
        void OnItemClick(View itemview, int position);
    }

    public KhachHangAdapter(ArrayList<KhachHang> danhsachkh, Context context) {
        this.danhsachkh = danhsachkh;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemview = inflater.inflate(R.layout.khachhang_1row, viewGroup, false);
        return new ViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        int i = position + 1;
        viewHolder.stt.setText(i + "");
        viewHolder.txtten.setText(danhsachkh.get(position).getTenkh());
        viewHolder.txtsdt.setText(danhsachkh.get(position).getSdtkh());
        viewHolder.txtmatkhau.setText(danhsachkh.get(position).getMatkhaukh());

    viewHolder.imv_delete.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Snackbar.make(((Activity)context).findViewById(R.id.rv_khachhang), "Xóa khách hàng " +danhsachkh.get(position).getTenkh(), 3500)
                    .setActionTextColor(Color.RED)
                    .setAction("Đồng ý!", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            KhachHang kh = danhsachkh.get(position);
                            khachhangDAO = new KhachHangDAO(context);
                            if (khachhangDAO.deleteKhachHang(kh)==-1){
                                Toast.makeText(context, "Xóa không thành công", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Toast.makeText(context, "Đã xóa khách hàng  " +danhsachkh.get(position).getTenkh(), Toast.LENGTH_SHORT).show();
                            danhsachkh.clear();
                            danhsachkh.addAll(khachhangDAO.getAllkhachhang());
                            KhachHangAdapter.this.notifyDataSetChanged();
                        }
                    }).show(); }});

    viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            AlertDialog.Builder alertdialog = new AlertDialog.Builder(v.getContext());
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            v = inflater.inflate(R.layout.alert_suakhachhang, null);
            alertdialog.setView(v);

            edtedittenkh = v.findViewById(R.id.edtedittenkh);
            final TextView tveditsdtkh = v.findViewById(R.id.tveditsdtkh);
            edteditmatkhaukh = v.findViewById(R.id.edteditmatkhaukh);
            edteditrematkhaukh = v.findViewById(R.id.edteditrematkhaukh);
            KhachHang kh = danhsachkh.get(position);
            // Set lên edittext sẵn khi click chỉnh sửa
            edtedittenkh.setText(kh.getTenkh());
            tveditsdtkh.setText("Chỉnh sửa số điện thoại " + kh.getSdtkh());
            alertdialog.setPositiveButton("Cập nhật", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    KhachHang kh = danhsachkh.get(position);
                    khachhangDAO = new KhachHangDAO(context);
                    kh.setTenkh(edtedittenkh.getText().toString());
                    kh.setSdtkh(tveditsdtkh.getText().toString());
                    kh.setMatkhaukh(edteditmatkhaukh.getText().toString());
                    String ten = edtedittenkh.getText().toString();
                    String mk = edteditmatkhaukh.getText().toString();
                    String remk = edteditrematkhaukh.getText().toString();
                    if (ten.length() == 0 || mk.length() == 0 || remk.length() == 0) {
                        Toast.makeText(context, "Chưa nhập thông tin", Toast.LENGTH_SHORT).show();
                    } else if (!mk.equals(remk)) {
                        Toast.makeText(context, "Xác nhận mật khẩu chưa chính xác", Toast.LENGTH_SHORT).show();
                    } else {
                        if (khachhangDAO.updateKhachHang(kh) == -1) {
                            Toast.makeText(context, "Cập nhật không thành công", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Toast.makeText(context, "Đã cập nhật lại " + danhsachkh.get(position).getTenkh(), Toast.LENGTH_SHORT).show();
                        danhsachkh.clear();
                        danhsachkh.addAll(khachhangDAO.getAllkhachhang());
                        KhachHangAdapter.this.notifyDataSetChanged();}

                }
            });
            alertdialog.setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertdialog.show();
            return false;
        }
    });
    }

    @Override
    public int getItemCount() {
        return danhsachkh.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtten, txtsdt, txtmatkhau, stt;
        ImageView imv_delete;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imv_delete = (ImageView)itemView.findViewById(R.id.imv_delete);
            stt = (TextView)itemView.findViewById(R.id.stt);
            txtten =(TextView)itemView.findViewById(R.id.tenkh_1row);
            txtsdt = (TextView)itemView.findViewById(R.id.sdtkh_1row);
            txtmatkhau = (TextView)itemView.findViewById(R.id.matkhaukh_1row);
        }
    }
}
