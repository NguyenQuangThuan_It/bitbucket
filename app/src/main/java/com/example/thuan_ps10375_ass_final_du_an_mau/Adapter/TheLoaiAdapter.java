package com.example.thuan_ps10375_ass_final_du_an_mau.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.thuan_ps10375_ass_final_du_an_mau.DAO.TheLoaiDAO;
import com.example.thuan_ps10375_ass_final_du_an_mau.Model.TheLoai;
import com.example.thuan_ps10375_ass_final_du_an_mau.R;

import java.util.ArrayList;

public class TheLoaiAdapter extends RecyclerView.Adapter<TheLoaiAdapter.ViewHolder> {
    ArrayList<TheLoai> danhsachtheloai;
    Context context;
    TheLoaiDAO theloaiDAO;

    public  static  View.OnClickListener listener;
    public TheLoaiAdapter(ArrayList<TheLoai> danhsachtheloai, Context context){
        this.danhsachtheloai = danhsachtheloai;
        this.context = context;
    }
    @NonNull
    @Override
    public TheLoaiAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemview = inflater.inflate(R.layout.theloai_1row, viewGroup, false);
        return new ViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull TheLoaiAdapter.ViewHolder viewHolder, final int position) {
        int i = position + 1;
        viewHolder.txtstt.setText(i + "");
        viewHolder.txttheloai.setText(danhsachtheloai.get(position).getTenTheLoai());
        viewHolder.txtvitri.setText(danhsachtheloai.get(position).getViTri());
        viewHolder.txtmaloai.setText(danhsachtheloai.get(position).getMaTheLoai());

        viewHolder.imv_edittheloai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertdialog = new AlertDialog.Builder(v.getContext());
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                v = inflater.inflate(R.layout.alert_suatheloai, null);
                alertdialog.setView(v);

                TheLoai tl = danhsachtheloai.get(position);
                theloaiDAO = new TheLoaiDAO(context);
                final TextView tveditmatheloai = v.findViewById(R.id.tvmatheloai);
                final EditText edtedittentheloai = v.findViewById(R.id.edtedittentheloai);
                final EditText edteditvitri = v.findViewById(R.id.edteditvitri);
                // Set lên edittext sẵn khi click chỉnh sửa
                tveditmatheloai.setText("Chỉnh sửa mã " + tl.getMaTheLoai());
                edtedittentheloai.setText(tl.getTenTheLoai());
                edteditvitri.setText(tl.getViTri());

                alertdialog.setPositiveButton("Chỉnh sửa", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String ten = edtedittentheloai.getText().toString();
                        String vitri = edteditvitri.getText().toString();
                        TheLoai tl = danhsachtheloai.get(position);
                        theloaiDAO = new TheLoaiDAO(context);
                        tl.setTenTheLoai(edtedittentheloai.getText().toString());
                        tl.setViTri(edteditvitri.getText().toString());
                        if(ten.length() ==0 || vitri.length() == 0){
                            Toast.makeText(context, "Chưa nhập thông tin", Toast.LENGTH_SHORT).show();
                        }else{
                            if(theloaiDAO.updateTheLoai(tl) == -1){
                                Toast.makeText(context, "Cập nhật không thành công", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Toast.makeText(context, "Đã cập nhật lại " + danhsachtheloai.get(position).getTenTheLoai(), Toast.LENGTH_SHORT).show();
                        }
                        danhsachtheloai.clear();
                        danhsachtheloai.addAll(theloaiDAO.getAllTheLoai());
                        TheLoaiAdapter.this.notifyDataSetChanged();
                    }
                });
                alertdialog.setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertdialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return danhsachtheloai.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txttheloai, txtvitri, txtstt, txtmaloai;
        ImageView imv_edittheloai;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imv_edittheloai = (ImageView)itemView.findViewById(R.id.imv_edittheloai);
            txtmaloai = (TextView)itemView.findViewById(R.id.matheloai_1row);
            txtstt = (TextView)itemView.findViewById(R.id.stttheloai);
            txttheloai =(TextView)itemView.findViewById(R.id.tentheloai_1row);
            txtvitri = (TextView)itemView.findViewById(R.id.vitri_1row);
        }
    }
}
