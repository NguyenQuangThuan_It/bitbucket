package com.example.thuan_ps10375_ass_final_du_an_mau.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.thuan_ps10375_ass_final_du_an_mau.DAO.SachDAO;
import com.example.thuan_ps10375_ass_final_du_an_mau.Model.Sach;
import com.example.thuan_ps10375_ass_final_du_an_mau.R;

import java.util.ArrayList;

public class SachAdapter extends RecyclerView.Adapter<SachAdapter.ViewHolder> {
    ArrayList<Sach> danhsachsach;
    Context context;
    SachDAO sachDAO;

    public SachAdapter(ArrayList<Sach> danhsachsach, Context context){
        this.danhsachsach = danhsachsach;
        this.context = context;
    }
    @NonNull
    @Override
    public SachAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemview = inflater.inflate(R.layout.sach_1row, viewGroup, false);
        return new ViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull SachAdapter.ViewHolder viewHolder, final int position) {
        int i = position + 1;
        viewHolder.txtsttsach.setText(i + "");
        viewHolder.txtmasach.setText(danhsachsach.get(position).getMasach());
        viewHolder.txtsach.setText(danhsachsach.get(position).getTensach());
        viewHolder.txtsoluong.setText(danhsachsach.get(position).getSoluong());
        viewHolder.txtgia.setText(danhsachsach.get(position).getGiabia());

        viewHolder.imv_deletesach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(((Activity)context).findViewById(R.id.rv_sach), "Xóa quyển " +danhsachsach.get(position).getTensach(), 3500)
                        .setActionTextColor(Color.RED)
                        .setAction("Đồng ý", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                    Sach sa = danhsachsach.get(position);
                                    sachDAO = new SachDAO(context);
                                    if (sachDAO.deletesach(sa)==-1){
                                        Toast.makeText(context, "Xóa không thành công", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    Toast.makeText(context, "Đã xóa quyển  " +danhsachsach.get(position).getTensach(), Toast.LENGTH_SHORT).show();
                                    danhsachsach.clear();
                                    danhsachsach.addAll(sachDAO.laytatcasach());
                                    SachAdapter.this.notifyDataSetChanged();
                                }
                            }).show(); }});


        viewHolder.imv_editsach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertdialog = new AlertDialog.Builder(v.getContext());
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                v = inflater.inflate(R.layout.alert_suasach, null);
                alertdialog.setView(v);

                Sach tl = danhsachsach.get(position);
                sachDAO = new SachDAO(context);
                final TextView tvmasach = v.findViewById(R.id.tvmasach);
                final TextView tvtheloaisach = v.findViewById(R.id.tvtheloaisach);
                final EditText edtedittensach = v.findViewById(R.id.edtedittensach);
                final EditText edteditsoluong = v.findViewById(R.id.edteditsoluong);
                final EditText edteditgiasach = v.findViewById(R.id.edteditgia);
                // Set lên edittext sẵn khi click chỉnh sửa
                tvmasach.setText("Chỉnh sửa sách " + tl.getMasach());
                tvtheloaisach.setText("Thể loại: " + tl.getMatheloai());
                edtedittensach.setText(tl.getTensach());

                alertdialog.setPositiveButton("Chỉnh sửa", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String tensach = edtedittensach.getText().toString();
                        String soluong = edteditsoluong.getText().toString();
                        String giasach = edteditgiasach.getText().toString();
                        Sach sa = danhsachsach.get(position);
                        sachDAO = new SachDAO(context);
                        sa.setTensach(edtedittensach.getText().toString());
                        sa.setSoluong(edteditsoluong.getText().toString());
                        sa.setGiabia(edteditgiasach.getText().toString());
                        if(tensach.length() ==0 || soluong.length() == 0 || giasach.length() == 0){
                            Toast.makeText(context, "Chưa nhập thông tin", Toast.LENGTH_SHORT).show();
                        }else{
                            if(sachDAO.updateSach(sa) == -1){
                                Toast.makeText(context, "Cập nhật không thành công", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Toast.makeText(context, "Đã cập nhật lại " + danhsachsach.get(position).getTensach(), Toast.LENGTH_SHORT).show();
                        }
                        danhsachsach.clear();
                        danhsachsach.addAll(sachDAO.laytatcasach());
                        SachAdapter.this.notifyDataSetChanged();
                    }
                });
                alertdialog.setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertdialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return danhsachsach.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtsach, txtsoluong, txtgia, txtsttsach, txtmasach;
        ImageView imv_editsach, imv_deletesach;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imv_editsach = (ImageView)itemView.findViewById(R.id.imv_editsach);
            imv_deletesach = (ImageView)itemView.findViewById(R.id.imv_deletesach);
            txtmasach = (TextView)itemView.findViewById(R.id.masach_1row);
            txtsach = (TextView)itemView.findViewById(R.id.tensach_1row);
            txtsttsach = (TextView)itemView.findViewById(R.id.sttsach) ;
            txtsoluong =(TextView)itemView.findViewById(R.id.soluongsach_1row);
            txtgia = (TextView)itemView.findViewById(R.id.giasach_1row);
        }
    }
}
