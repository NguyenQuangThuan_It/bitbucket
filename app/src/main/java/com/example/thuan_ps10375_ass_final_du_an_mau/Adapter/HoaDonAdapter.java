package com.example.thuan_ps10375_ass_final_du_an_mau.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thuan_ps10375_ass_final_du_an_mau.DAO.HoaDonDAO;
import com.example.thuan_ps10375_ass_final_du_an_mau.Model.HoaDon;
import com.example.thuan_ps10375_ass_final_du_an_mau.R;

import java.util.ArrayList;


public class HoaDonAdapter extends RecyclerView.Adapter<HoaDonAdapter.ViewHolder> {
    ArrayList<HoaDon> danhsachhoadon;
    Context context;
    double thanhtien =0;

    HoaDonDAO hoadonDAO;

    public  static  View.OnClickListener listener;
    public HoaDonAdapter(ArrayList<HoaDon> danhsachhoadon, Context context){
        this.danhsachhoadon = danhsachhoadon;
        this.context = context;
    }
    @NonNull
    @Override
    public HoaDonAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemview = inflater.inflate(R.layout.hoadon_1row, viewGroup, false);
        return new ViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull HoaDonAdapter.ViewHolder viewHolder, final int position) {
        final int i = position + 1;
        viewHolder.txtstt.setText(i + "");
        viewHolder.txtmahoadon.setText(danhsachhoadon.get(position).getMahoadon());
        viewHolder.txtngaythem.setText((CharSequence) danhsachhoadon.get(position).getNgaythemdon());

        viewHolder.imv_hoadon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertdialog = new AlertDialog.Builder(v.getContext());
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                v = inflater.inflate(R.layout.alert_hoadonchitiet, null);
                alertdialog.setView(v);
                /////////////////////


                HoaDon hd = danhsachhoadon.get(position);
                hoadonDAO = new HoaDonDAO(context);
                final TextView tvmahoadon = v.findViewById(R.id.tv_mahoadon);
                final TextView tvthanhtien = v.findViewById(R.id.tv_thanhtien);
                final EditText edtngaythemdon = v.findViewById(R.id.edteditngaythemdon);
                final EditText edtsoluongsach = v.findViewById(R.id.edteditsoluongsach);
                final EditText edtmasach = v.findViewById(R.id.edteditmasach);
                // Set lên edittext sẵn khi click chỉnh sửa
                tvmahoadon.setText("Chỉnh sửa mã " + hd.getMahoadon());
                edtngaythemdon.setText(hd.getNgaythemdon());
                edtsoluongsach.setText(hd.getSlsachmua());
                edtmasach.setText(hd.getMasach());

                alertdialog.setPositiveButton("Chỉnh sửa", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String  ngaythemdon = edtngaythemdon.getText().toString();
                        String soluongsach = edtsoluongsach.getText().toString();
                        String masach = edtmasach.getText().toString();
                        HoaDon hd = danhsachhoadon.get(position);
                        hoadonDAO = new HoaDonDAO(context);
                        //thanhtien = thanhtien + hd.getSlsachmua() * hd.getMasach().get
                        hd.setNgaythemdon(edtngaythemdon.getText().toString());
                        hd.setMasach(edtmasach.getText().toString());
                        hd.setSlsachmua(edtsoluongsach.getText().toString());
                        if(ngaythemdon.length() ==0 || masach.length() == 0
                         || soluongsach.length() ==0){
                            Toast.makeText(context, "Chưa nhập thông tin", Toast.LENGTH_SHORT).show();
                        }else{
                            if(hoadonDAO.updateHoaDon(hd) == -1){
                                Toast.makeText(context, "Cập nhật không thành công", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Toast.makeText(context, "Đã cập nhật lại " + danhsachhoadon.get(position).getMahoadon(), Toast.LENGTH_SHORT).show();
                        }
                        danhsachhoadon.clear();
                        danhsachhoadon.addAll(hoadonDAO.getAllhoadon());
                        HoaDonAdapter.this.notifyDataSetChanged();
                    }
                });
                alertdialog.setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertdialog.show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return danhsachhoadon.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtngaythem, txtstt, txtmahoadon;
        ImageView imv_hoadon;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtmahoadon = (TextView)itemView.findViewById(R.id.mahoadon_1row);
            txtstt = (TextView)itemView.findViewById(R.id.stthoadon);
            txtngaythem =(TextView)itemView.findViewById(R.id.ngaythem_1row);
            imv_hoadon = (ImageView)itemView.findViewById(R.id.imv_hoadonct);
        }
    }
}
