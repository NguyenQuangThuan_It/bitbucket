package com.example.thuan_ps10375_ass_final_du_an_mau.Adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.thuan_ps10375_ass_final_du_an_mau.Fragment_Sach_Main;
import com.example.thuan_ps10375_ass_final_du_an_mau.Fragment_TheLoai;


public class Tablayout_Sach extends FragmentStatePagerAdapter {

    private String listTab[] = {"Thể loại", "Sách"};
    private Fragment_TheLoai theloaiFragment;
    private Fragment_Sach_Main sachmainFragment;
    public Tablayout_Sach(FragmentManager fm) {
        super(fm);
        theloaiFragment = new Fragment_TheLoai();
        sachmainFragment = new Fragment_Sach_Main();
    }

    @Override
    public Fragment getItem(int i) {
        if(i==0){
            return theloaiFragment;
        }else if(i==1){
            return sachmainFragment;
        }else
        {}
        return null;
    }

    @Override
    public int getCount( ) {
        return listTab.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return listTab[position];
    }
}
