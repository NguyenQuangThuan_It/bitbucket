package com.example.thuan_ps10375_ass_final_du_an_mau;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;


import com.example.thuan_ps10375_ass_final_du_an_mau.Adapter.KhachHangAdapter;
import com.example.thuan_ps10375_ass_final_du_an_mau.DAO.KhachHangDAO;
import com.example.thuan_ps10375_ass_final_du_an_mau.Model.KhachHang;

import java.util.ArrayList;

public class DangNhap extends AppCompatActivity {
    CheckBox luumatkhau;
    EditText edtuser, edtpass, edtregistten, edtregistsdt, edtregistmatkhau, edtregistrematkhau;
    Button btlogin, btregister;
    SharedPreferences luutru;
    ArrayList<KhachHang> list;
    RecyclerView rv_khachhang;
    KhachHangAdapter khachhangAdapter;
    KhachHangDAO khachhangDAO;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dangnhap);

        setTitle("ĐĂNG NHẬP");

        khachhangDAO = new KhachHangDAO(getApplicationContext());

        edtregistten = findViewById(R.id.edtregistten);
        edtregistmatkhau = findViewById(R.id.edteditmatkhaukh);
        edtregistrematkhau = findViewById(R.id.edtregistrematkhau);
        edtregistsdt = findViewById(R.id.edtregistsdt);
        btlogin = findViewById(R.id.btlogin);
        btregister = findViewById(R.id.btregister);
        edtpass = findViewById(R.id.edtpass);
        edtuser = findViewById(R.id.edtuser);
        luumatkhau = findViewById(R.id.luumatkhau);
        rv_khachhang = findViewById(R.id.rv_khachhang);
        list = new ArrayList<>();
        khachhangDAO = new KhachHangDAO(this);
        khachhangAdapter = new KhachHangAdapter(list, this);
        luutru = getSharedPreferences("filematkhau", Context.MODE_PRIVATE);
        // Nạp sẵn thông tin lên Edittext cho lần thứ 2
        // defValue là khoảng trắng nếu như lần đầu tiên vào ứng dụng, user và pass chưa từng được lưu
        if(luutru.getBoolean("save_information", false)) {
            edtuser.setText(luutru.getString("username_information", ""));
            edtpass.setText(luutru.getString("password_information", ""));
            luumatkhau.setChecked(true);
        }

    }

    public void anhxa(){

    }

     public void login(View v){
         String user = edtuser.getText().toString();
         String pass = edtpass.getText().toString();
         SharedPreferences.Editor editor = luutru.edit();
         Boolean checklogin = khachhangDAO.checkLoginKhachHang(user, pass);
         if(user.isEmpty() || pass.isEmpty()) {
             Toast.makeText(getApplicationContext(), "Tài khoản và mật khẩu không được bỏ trống", Toast.LENGTH_SHORT).show();
         }else if(checklogin == true){
                     Intent intent = new Intent(DangNhap.this, MainActivity.class);
                     startActivity(intent);
                     Toast toast = Toast.makeText(DangNhap.this, "Đăng nhập thành công", Toast.LENGTH_SHORT);
                     toast.show();
                     if (luumatkhau.isChecked()) {
                         editor.putString("username_information", user);
                         editor.putString("password_information", pass);
                     }
                     editor.putBoolean("save_information", luumatkhau.isChecked());
                     editor.commit();
             } else if(checklogin==false){
             Toast.makeText(DangNhap.this, "Tài khoản hoặc mật khẩu không chính xác!", Toast.LENGTH_LONG).show();
             return;
         }
}
            public void regist(View v){

                AlertDialog.Builder builder = new AlertDialog.Builder(DangNhap.this);
                LayoutInflater inflater = DangNhap.this.getLayoutInflater();
                v = inflater.inflate(R.layout.doimatkhau, null);
                builder.setView(v);

                edtregistten = v.findViewById(R.id.edtregistten);
                edtregistmatkhau = v.findViewById(R.id.edtregistmk);
                edtregistrematkhau = v.findViewById(R.id.edtregistrematkhau);
                edtregistsdt = v.findViewById(R.id.edtregistsdt);
                builder.setPositiveButton("Đăng kí", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String sodienthoai = edtregistsdt.getText().toString();
                        Boolean checksdt = khachhangDAO.checksdtkhachhang(sodienthoai);
                        KhachHang kh = new KhachHang();
                        kh.setTenkh(edtregistten.getText().toString());
                        kh.setSdtkh(edtregistsdt.getText().toString());
                        kh.setMatkhaukh(edtregistmatkhau.getText().toString());
                        if(edtregistten.getText().length()==0 ||
                                edtregistsdt.getText().length() == 0 ||
                                edtregistmatkhau.getText().length() == 0 ||
                                edtregistrematkhau.getText().length() == 0){
                            Toast.makeText(DangNhap.this, "Chưa nhập thông tin", Toast.LENGTH_LONG).show();
                        } else if(checksdt==false){
                            Toast.makeText(DangNhap.this, "Số điện thoại đã tồn tại", Toast.LENGTH_LONG).show();
                            return;
                        } else if(!edtregistmatkhau.getText().toString().trim().equals(edtregistrematkhau.getText().toString())) {
                            Toast.makeText(DangNhap.this, "Xác nhận mật khẩu chưa chính xác!", Toast.LENGTH_SHORT).show();
                        }else if(khachhangDAO.addKhachHang(kh)==-1){
                                Toast.makeText(DangNhap.this, "Đăng kí không thành công", Toast.LENGTH_LONG).show();
                                return;
                            }else
                            Toast.makeText(DangNhap.this, "Đăng kí thành công", Toast.LENGTH_SHORT).show();
                            list = khachhangDAO.getAllkhachhang();
                            khachhangAdapter = new KhachHangAdapter(list, DangNhap.this);
                    }
                });
                builder.setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
}
